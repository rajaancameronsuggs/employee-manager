Root Project:<br />


**Frontend**<br />
ng new _project_name_<br />
Yes to Routing<br />
Choose a styling language (CSS)<br />
cd _project_name_<br />

**Compilation:**<br />
ng serve<br />
Ran locally: localhost:4200<br />

**Frameworks:**<br />
npm install bootstrap<br />

**Components**<br />
ng g c _component_name_<br />

**Classes**<br />
ng g class _class_name_<br />

**Service**<br />
ng g service _service_name_<br />





**Backend**<br />
Spring Initializer or https://start.spring.io/<br />
Fill in settings: <br />
Maven, <br />
Artifact/Name,<br /> 
Dependencies: Spring Web, PostGreSql Driver, Spring Data JPA, Spring Boot Dev Tools<br />
Generate and command line move/mv into git repository<br />

CORS error handled<br />
