import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {

  id!: number;
  employee: Employee = new Employee;
  // inject the service into the update component
  // inject the activated route into the update
  constructor(private employeeService: EmployeeService, private route: ActivatedRoute, private router: Router) {

   }

  ngOnInit(): void {
  }

  updateEmployee() {
    this.id = this.route.snapshot.params['id'];
    this.employeeService.updateEmployee(this.employee, this.id).subscribe( data => 
      {
        this.employee = data;
        this.goToEmployeeList();
      },
      error => console.log(error));
  }

  onSubmit() {
    console.log(this.employee);
    this.updateEmployee();

  }

  goToEmployeeList() {
    this.router.navigate(['/employees/']);
  }
}
