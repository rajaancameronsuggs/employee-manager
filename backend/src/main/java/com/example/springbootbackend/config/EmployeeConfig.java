package com.example.springbootbackend.config;

import com.example.springbootbackend.model.Employee;
import com.example.springbootbackend.repository.EmployeeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

@Configuration
public class EmployeeConfig {
    @Bean
    CommandLineRunner commandLineRunner(EmployeeRepository repo) {
        return args -> {
            Employee pablo = new Employee(
                    "Pablo",
                    "Sanchez",
                    "pablo@gmail.com",
                    "Full Stack Engineer",
                    75000
            );
            Employee ferb = new Employee(
                    "Ferb",
                    "Fletcher",
                    "ferb@gmail.com",
                    "Chief Executive Officer",
                    150000
            );
            Employee trunks = new Employee(
                    "Trunks",
                    "",
                    "trunks@outlook.oh",
                    "Senior Financial Analyst",
                    120000

            );
            Employee dark = new Employee(
                    "Dark",
                    "Pit",
                    "darkpit@yahoo.com",
                    "Human Resources",
                    55000

            );
            Employee raven = new Employee(
                    "Raven",
                    "Titan",
                    "trunks@hubspot.uk",
                    "Legal Associate Attorney",
                    100000
            );
            Employee nala = new Employee(
                    "Nala",
                    "Lioness",
                    "nala@salesforce.fr",
                    "Industrial Designer",
                    58600
            );
            Employee levi = new Employee(
                    "Levi",
                    "Ackerman",
                    "levi@aol.no",
                    "Sales Member",
                    44700
            );
            // saveAll saves a list
            ArrayList<Employee> employees = new ArrayList<>();
            employees.add(pablo);
            employees.add(ferb);
            employees.add(trunks);
            employees.add(dark);
            employees.add(raven);
            employees.add(nala);
            employees.add(levi);
            repo.saveAll(
                    employees
            );
        };
    }
}